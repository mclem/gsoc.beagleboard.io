.. _gsoc-idea-linux-kernel-improvements:

Linux kernel improvements
#########################

Below are the projects that comes under linux kernel improvements category.

.. card:: 

    :fab:`linux;pst-color-secondary` **BeagleBone Cape (add-on board) compatibility layer (BB-CCL)** :bdg-danger:`High priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`
    ^^^^

    - **Goal:** Update the cape compatibility layer for BeagleBone Black, AI-64, and newer boards to support more kernel versions.
    - **Hardware Skills:** UART, I2C, SPI, ADC, GPIO and PWM
    - **Software Skills:** C, Python, DeviceTree, Linux kernel
    - **Possible Mentors:** Deepak Khatri, jkridner
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** various
    - **References:** 
        - `Beagleboard:BeagleBone cape interface spec <https://docs.beagleboard.org/latest/boards/capes/cape-interface-spec.html>`_
        - `Device tree how-to blog post <https://beagleboard.org/blog/2022-02-15-using-device-tree-overlays-example-on-beaglebone-cape-add-on-boards>`_
        - `GSoC 2020 project page <https://elinux.org/BeagleBoard/GSoC/2020_Projects/Cape_Compatibility>`_
        - `lorforlinux's GSoC project documentation website <https://lorforlinux.github.io/GSoC2020_BeagleBoard.org/>`_

    ++++

    .. button-link:: https://example.com
      :color: secondary
      :expand:

      :fab:`gitlab;pst-color-light` Discuss on forum

.. card:: 

    :fab:`linux;pst-color-secondary` **Xenomai vs Preempt-RT**
    ^^^^

    In the growing market of embedded Linux audio devices, engineers more and more face the 
    question "how fast is fast enough?". There is need to provide a framework for testing 
    responsiveness (and consequently minimum latency) for a given platform and evaluate 
    whether it meets the requirements for the application at hand. In 2010, the paper 
    "How fast is fast enough?" by Brown and Martin compared average and worst-case performance 
    of Preempt-rt Linux vs Xenomai on an old BeagleBoard. In this project you will develop code 
    for a test setup that allows to reproduce these tests on more modern SoCs in order to 
    obtain an indirect measure of how much Preempt-rt has improved over the past few years, 
    what is today the gap with Xenomai and reflect on how this impacts specific applications. 
    Goal: Produce a testing suite that allows to evaluate interrupt latency and CPU performance 
    on Xenomai vs Preempt-rt
    
    - **Hardware Skills:** none
    - **Software Skills:** C, Linux kernel, Xenomai
    - **Possible Mentors:** giuliomoro
    - **Expected Size of Project:** 350 hrs
    - **Rating:** Medium
    - **Upstream Repository:** https://xenomai.org

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`

.. card:: 

    :fab:`linux;pst-color-secondary` **Xenomai kernel for most recent BB boards**
    ^^^^

    `Xenomai <http://xenomai.org/>`_ is a co-kernel for Linux that allows selected threads on 
    the board to bypass the Linux kernel and deliver hard real-time performance. This is used 
    worldwide in such diverse sectors as manufacturing processes, drones and audio (e.g.: 
    `the Bela project <https://bela.io/>`_ on Beagleaboard boards). The Xenomai core is 
    currently maintained by Siemens and interactions happen through the `official mailing list 
    <https://xenomai.org/mailman/listinfo/xenomai/>`_. Board-specific support is only offered on 
    a small set of SoCs, but it's mostly down to the maintainers of individual boards. Kernels for 
    the Beagleboard family are regularly patched and released by `rcn-ee`, who also occasionally 
    releases Xenomai versions. While 4.9, 4.14 and 4.19 (and newer versions) have been working 
    fine on AM335x devices (e.g.: BeagleBone Black, PocketBeagle), these do not run properly on other 
    devices (e.g.: AM572x-based ones such as Beaglebone-AI). The scope of this project is to offer 
    support for Xenomai kernels across the Beagleboard family and implement a workflow that makes 
    maintenance easier.

    - **Goal:** Fix Xenomai support for newer BB boards, make future maintenance easier.
    - **Hardware Skills:** none
    - **Software Skills:** C, Linux kernel, Xenomai
    - **Possible Mentors:** giuliomoro, Dhruva Gole
    - **Expected Size of Project:** 350 hrs
    - **Rating: Medium**
    - **Upstream Repository:** https://evlproject.org/overview/ https://xenomai.org
    - **References:**
        - `Paper: How fast is fast enough? <https://picture.iczhiku.com/resource/paper/WyIEpzspsLtjRXnc.pdf>`_

    ++++

    :bdg-success:`Medium priority` :bdg-success:`Medium complexity` :bdg-danger-line:`Large size`