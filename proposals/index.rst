.. _gsoc-proposal-Himanshu kohale:

#########

.. tip:: 

    Checkout :ref:`gsoc-project-ideas` page to explore ideas and :ref:`gsoc-proposal-guide` page to write your own proposal.


.. toctree:: 
    :hidden:
  
    melta101
    drone_cape_for_beagle-v-fire
    suraj-sonawane
    template
    alecdenny
    matt-davison
    mc


