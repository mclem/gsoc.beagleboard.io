.. _gsoc-mentor-guide:

Mentor Guide
############

.. youtube:: 3J_eBuYxcyg
    :width: 100%
    :align: center

.. important::

    As a mentor you have to guide contributors through the entire process of GSoC, from learning
    about `BeagleBoard.org <https://www.beagleboard.org/>`_ community, to helping GSoC Contributors
    become familiar with the code base and testing practices, to finally releasing their code on
    `OpenBeagle <https://openbeagle.org/>`_ for the world to use!

    You will also need to be invited by an administrator to register on the GSoC site and request
    to be a mentor for `BeagleBoard.org <https://www.beagleboard.org/>`_.

Who Are Mentors?
****************

`Mentors <https://google.github.io/gsocguides/mentor/>`_ are people from the community who volunteer to work
with a GSoC contributor. Mentors provide guidance such as pointers to useful documentation, code reviews, etc.
In addition to providing GSoC contributors with feedback and pointers, a mentor acts as an ambassador to help
GSoC contributors integrate into their project’s community. `BeagleBoard.org <https://www.beagleboard.org/>`_
always assigns more than one mentor to each of GSoC contributor. Many members of `BeagleBoard.org <https://www.
beagleboard.org/>`_ community also provides guidance to GSoC contributors without mentoring in an “official”
capacity, as much as they would answer anyone’s questions on our `Discord <https://bbb.io/gsocchat>`_ and our
`Forum <https://bbb.io/gsocml>`_.

Idea Submission Process
***********************

Mentors should:

1. Submit project ideas to our `Forum <https://bbb.io/gsocml>`_ and then
2. Contribute an update to our :ref:`gsoc-project-ideas` page using our :ref:`gsoc-site-editing-guide` to promote their idea to contributors.

Only ideas deemed by administrators as being sufficiently supported by qualified mentors will be merged.

.. admonition:: Remember

    BeagleBoard.org mentored GSoC projects are supposed to be for software projects that service the Beagle and general open source
    embedded systems community, not theses, how-to guides or what I did over my summer vacation ideas.

Prospective mentors, students will use our `Discord <https://bbb.io/gsocchat>`_ and `Forum <https://bbb.io/gsocml>`_
to make contact with you, so be sure to provide up-to-date information. Please feel free to add yourself on mentors page and we will monitor
and police that list. Acceptance as an official mentor with the ability to rate proposals and grade contributors will come via the Google system.
We will only approve official mentors who have a proven track record with Beagle, but welcome all community members to provide guidance to both
mentors and contributors to best serve the community as a whole. Don’t be shy, and don’t be offended when we edit. We are thrilled to have you on board!



.. tip::
    Check out the `official GSoC mentor guide <https://google.github.io/gsocguides/mentor/>`_ from Google.
